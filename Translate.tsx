import React, { PropTypes, Component } from 'react';
import * as fetch from 'isomorphic-fetch';
import * as ls from 'local-storage';

class Translate extends Component {
  constructor(props) {
    super(props);
    this.state = { translation: ' ', ref: new Date().getTime() * (Math.random() * 10) };
  }

  componentDidMount() {
    this.getTranslation(this.props.children);
  }

  static getFile(lang = 'en') {
    return new Promise((resolve, reject) => {
      const cache = (ls.get(`planner_lang_cache_${lang}`)) ?
        JSON.parse(ls.get(`planner_lang_cache_${lang}`)) : null;
      if (!cache || cache.update < new Date().getTime()) {
        fetch.default(`/lang/i18/${lang}.json`)
        .then((response) => response.json())
        .then((response) => {
          ls.set(`planner_lang_cache_${lang}`, JSON.stringify({
            update: new Date().getTime() + 10 * 60 * 1000,
            data: response
          }));
          resolve(response);
        });
      } else {
        resolve(cache.data);
      }
    });
  }

  capitalize(str) {
    if (this.props.capitalize) {
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return str;
  }

  uppercase(str) {
    if (this.props.uppercase) {
      return str.toUpperCase();
    }
    return str;
  }

  getTranslation(key, lang=navigator.language) {
    if (this.refs[this.state.ref]) {
      this.setState({ translation: key });
      return new Promise((resolve) => {
        Translate.getFile(lang)
        .then((file) => {
          if (file.hasOwnProperty(key)) {
            this.setState({ translation: this.uppercase(this.capitalize(file[key])) });
          } else {
            this.setState({ translation: key });
          }
        })
        .catch((err) => {
          this.setState({ translation: key });
        })
      });
    }
  }

  static get(keys, lang=navigator.language) {
    return new Promise((resolve) => {
      Translate.getFile(lang)
      .then((file) => {
        const fetched = keys.reduce((total, item) => {
          const key = (file.hasOwnProperty(item)) ?
                  file[item] :
                  item;
          total[item] = key;
          return total;
        }, {});
        resolve(fetched);
      })
      .catch((err) => {
        resolve(keys);
      })
    });
  }

  render() {
    return (<span ref={this.state.ref} style={this.props.style}>{(this.state && this.state.translation) ? this.state.translation : ' '}</span>);
  }
}

export default Translate;
