// Type definitions for material-ui v0.17.51
// Project: https://github.com/callemall/material-ui
// Definitions by: Nathan Brown <https://github.com/ngbrown>, Igor Belagorudsky <https://github.com/theigor>, Ali Taheri Moghaddar <https://github.com/alitaheri>, Oliver Herrmann <https://github.com/herrmanno>, Daniel Roth <https://github.com/DaIgeb>, Aurelién Allienne <https://github.com/allienna>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.2

/// <reference types="react" />
/// <reference types="react-addons-linked-state-mixin" />

declare module "trunkrs-components" {
    export namespace Toolbar {
        interface ToolbarProps {
            className?: string;
            noGutter?: boolean;
            style?: React.CSSProperties;
        }
        export class Toolbar extends React.Component<ToolbarProps, {}> {
        }

        interface ToolbarGroupProps {
            className?: string;
            firstChild?: boolean;
            float?: "left" | "right";
            lastChild?: boolean;
            style?: React.CSSProperties;
        }
        export class ToolbarGroup extends React.Component<ToolbarGroupProps, {}> {
        }

        interface ToolbarSeparatorProps {
            className?: string;
            style?: React.CSSProperties;
        }
        export class ToolbarSeparator extends React.Component<ToolbarSeparatorProps, {}> {
        }

        interface ToolbarTitleProps extends React.HTMLAttributes<{}>, React.Props<ToolbarTitle> {
            className?: string;
            style?: React.CSSProperties;
            text?: string;
        }
        export class ToolbarTitle extends React.Component<ToolbarTitleProps, {}> {
        }
    }
}

declare module 'trunkrs-components/Toolbar' {
    export import Toolbar = __MaterialUI.Toolbar.Toolbar;
    export import ToolbarGroup = __MaterialUI.Toolbar.ToolbarGroup;
    export import ToolbarSeparator = __MaterialUI.Toolbar.ToolbarSeparator;
    export import ToolbarTitle = __MaterialUI.Toolbar.ToolbarTitle;
    export default Toolbar;
}

declare module 'trunkrs-components/Toolbar/Toolbar' {
    export import Toolbar = __MaterialUI.Toolbar.Toolbar;
    export default Toolbar;
}
